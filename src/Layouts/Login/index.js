import React ,{ useState,useEffect } from 'react'
import { Row, Col , Card } from 'antd';
import { Form, Icon, Input, Button,Checkbox } from 'antd';
import {Animated} from "react-animated-css";

function Index(props) {

    const [status, setStatus] = useState(true);

    const handleSubmit=(e)=>{
        e.preventDefault();
        props.form.validateFields((err, values) => {
          if (!err) {
            setStatus(false);
            setTimeout(function() {
                window.location.href='/main'
            }, 1000);
          }
        });
    };
  

    const { getFieldDecorator } = props.form;
      
    return (
        <Row type="flex" justify="space-around" align="middle" style={{height:"100%"  }}>
        <Col span={1} md={2} xl={6}>
        </Col>
          
            <Col span={14} md={12} xl={6}>
            <Animated animationIn="pulse" animationOut="fadeOut" animationInDuration={1000} animationOutDuration={1000} isVisible={status}>
                    <Card  style={{ width: "100%" , boxShadow:" 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"  }}>
                        <Form 
                        onSubmit={handleSubmit}
                        className="login-form">
                            
                                <center><img src="./img/logo_login.png" style={{width:"50%" , height:"50%"}}/></center>
                           
                            <Form.Item>
                            {getFieldDecorator('username', {
                                rules: [{ required: true, message: 'Please input your username!' }],
                            })(
                                <Input
                                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                placeholder="Username"
                                />,
                            )}
                            </Form.Item>
                            <Form.Item>
                            {getFieldDecorator('password', {
                                rules: [{ required: true, message: 'Please input your Password!' }],
                            })(
                                <Input
                                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                type="password"
                                placeholder="Password"
                                />,
                            )}
                            </Form.Item>
                            <Form.Item>
                            <Button type="primary" htmlType="submit" className="login-form-button" block>
                                Log in
                            </Button>
                        
                            </Form.Item>
                        </Form>
                    </Card>
                </Animated>
            </Col>
            
        <Col span={1} md={2} xl={6}>
        <div></div>
        </Col>
      </Row>
    );
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(Index);

export default WrappedNormalLoginForm

