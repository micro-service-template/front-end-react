import React ,{ useState,useEffect } from 'react'
import LoadingComponent from '../../Components/Loading';
import HamburgerMenu from 'react-hamburger-menu';
import { Drawer } from 'antd';
import Main from './main';
import { Button ,Divider, BackTop} from 'antd';
import './index.css';
import $ from "jquery";
function Index(props) {
    const [percent, setPercent] = useState(0);
    // const [data , setData] = useState();
    const [index , setIndex] = useState(1);
    const [status , setStatus] = useState(false);
    const [hamburger , setHamburger] = useState(false);
    useEffect(() => {
        if(index == 1){
            userAction();    
        }else if(index == 2){
            userAction2();
        }else{
            setStatus(false);
        }
    },[index]);

    const userAction = async () => {
        const response = await fetch('/api/home',{

        });
        const myJson = await response.json(); //extract JSON from the http response
        console.log(myJson);
        if(myJson){
            setPercent(percent+50);
            setIndex(index+1);
        }
     
    }
    const userAction2 = async () => {
        const response2 = await fetch('https://jsonplaceholder.typicode.com/posts/41');
        const myJson2 = await response2.json(); //extract JSON from the http response
        console.log(myJson2);
        if(myJson2){
            setPercent(percent+50)
            setIndex(index+1);
        }
       
    }

    const handleClick= ()=> {
        setHamburger(!hamburger)
    }

    function handleClickLogOut(){
        window.location='../';
    }

    function getYear(){
        var d = new Date();
        
        $(".move-area").mousemove(function(event) {
            var eye = $(".eye");
            var x = (eye.offset().left) + (eye.width() / 2);
            var y = (eye.offset().top) + (eye.height() / 2);
            var rad = Math.atan2(event.pageX - x, event.pageY - y);
            var rot = (rad * (180 / Math.PI) * -1) + 180;
            eye.css({
                '-webkit-transform': 'rotate(' + rot + 'deg)',
                '-moz-transform': 'rotate(' + rot + 'deg)',
                '-ms-transform': 'rotate(' + rot + 'deg)',
                'transform': 'rotate(' + rot + 'deg)'
            });
        });
        
        return d.getFullYear();
    }

    function getComponents(){
       console.log( window.location);
       
        return <Main  />;
        
    }
     

    return (
     
        <div className="move-area" style={{width:"100%", height:"100%"}}>
            <LoadingComponent value={percent} status={status}  />
            <div style={{ zIndex: "2", position:"absolute" , left:0 , right:0 ,width:"100%", height:"95%" }}>
       
           
                <div style={{ zIndex: "3", position:"absolute" , marginTop:"20px", marginLeft:"10px" }}>  
                  
                   
                    <HamburgerMenu
                        isOpen={hamburger}
                        menuClicked={handleClick}
                        width={30}
                        height={20}
                        strokeWidth={2}
                        rotate={0}
                        color='black'
                        borderRadius={10}
                        animationDuration={0.2}
                        style={{marginTop:"20px"}}
                    />
                    <Drawer  
                        title="Minion"
                        placement="right"
                        closable={false}
                        onClose={handleClick}
                        visible={hamburger}
                        >
                        <Button type="danger" onClick={(e)=>{handleClickLogOut()}} block>
                            Log Out
                        </Button>
                    </Drawer>
                   
                    
                   
                </div>
                <div style={{marginTop:"70px", width:"100%", height:"100%"  }}>
                    
                    <Divider > 
                        <img src="./img/logo_login.png" style={{zIndex: "-1", position:"absolute",width:"150px" 
                        , height:"150px" ,marginTop:"-121px" , marginLeft:"-73px"}}/>
                        <div class='.container'  style={{marginTop:"-60px"}}>
                            <div class='eye' style={{marginLeft:"5px"}}></div>
                            <div class='eye' style={{marginLeft:"6px"}}></div>
                        </div>
                    </Divider>
                    
                    {getComponents()}
                    <BackTop />
                    <Divider>&copy; Copyright {getYear()}</Divider>

                  
                
                </div>
               
            </div>
          
        </div>
    );
  }


export default Index

