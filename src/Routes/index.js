import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import LoginPage from '../Layouts/Login';
import MainPage from '../Layouts/Main';
import SubMainPage from '../Layouts/Main/SubMain';

export default class index extends Component {
   
    render() {
        return (
            <Router >
                <Route exact path="/" component={LoginPage} />
                <Route exact path="/main" component={MainPage} />
                <Route path="/main/submain" component={SubMainPage} />
            </Router>  
        )
    }
}
