import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'antd/dist/antd.css';
import './index.css';
import Route from './Routes';

function App() {
  return (
    <div style={{height:"100%" ,backgroundImage: `url('./img/background.jpg')`}} >
        <Route />
    </div>
  );
}

export default App;
