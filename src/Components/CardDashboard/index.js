import React ,{useState} from 'react'
import PropTypes from 'prop-types'
import { Card } from 'antd';
import { Menu, Dropdown, Button ,Icon } from 'antd';
import './index.css';
import BodyCard from './BodyCard';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
function Index(props) {

    const [backgroundColor,setBackgroundColor] = useState("white");

    const menu = (
        <Menu>
          <Menu.Item>
            <a target="_blank" rel="noopener noreferrer"  onClick={(e)=>changeBackGround("red")}>
                 # red
            </a>
          </Menu.Item>
          <Menu.Item>
            <a target="_blank" rel="noopener noreferrer"  onClick={(e)=>changeBackGround("yellow")}>
                 # yellow
            </a>
          </Menu.Item>
          <Menu.Item>
            <a target="_blank" rel="noopener noreferrer"  onClick={(e)=>changeBackGround("bright")}>
                 # bright
            </a>
          </Menu.Item>
          <Menu.Item>
            <a target="_blank" rel="noopener noreferrer"  onClick={(e)=>changeBackGround("blue")}>
                 # blue
            </a>
          </Menu.Item>
          <Menu.Item>
            <a target="_blank" rel="noopener noreferrer"  onClick={(e)=>changeBackGround("dark_blue")}>
                 # dark blue
            </a>
          </Menu.Item>
          <Menu.Item>
            <a target="_blank" rel="noopener noreferrer"  onClick={(e)=>changeBackGround("green")}>
                 # green
            </a>
          </Menu.Item>
          <Menu.Item>
            <a target="_blank" rel="noopener noreferrer"  onClick={(e)=>changeBackGround("purple")}>
                 # purple
            </a>
          </Menu.Item>
          <Menu.Item>
            <a target="_blank" rel="noopener noreferrer"  onClick={(e)=>changeBackGround("white")}>
                 # white
            </a>
          </Menu.Item>
        </Menu>
    );

    function changeBackGround(val){
        setBackgroundColor(val);    
    }

    const num = Math.floor((Math.random() * 10) + 1);

    return (
        <div>
            <Card className={backgroundColor} style={{height:"300px", borderColor:"transparent" }} hoverable> 
               
                <div align="right"><Dropdown overlay={menu} placement="bottomRight">
                    <Button type="primary" style={{ backgroundColor: "transparent" , borderColor:"transparent",color:"black"}}>:</Button>
                </Dropdown>
                </div>
                <div>
                  <div>Topic : {num}
                    <Link to={{ pathname: '/main/submain', state: { foo: 'bar'} }}>
                      <Icon type="edit" />
                    </Link>
                  </div>
                  <BodyCard id={num} />
                </div>
                
             </Card>
        </div>
    )
}



export default Index

