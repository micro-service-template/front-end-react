import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { VictoryChart ,VictoryBoxPlot  ,VictoryArea ,VictoryLine ,VictoryGroup ,VictoryBar} from 'victory';

function index(props) {


    function getGraph(){
        var body = null;
        if(props.id == 1 || props.id == 5){
            body = (<VictoryChart domainPadding={10}>
                        <VictoryBoxPlot
                            boxWidth={10}
                            whiskerWidth={5}
                            data={[
                                { x: 1, y: [1, 2, 3, 5] },
                                { x: 2, y: [3, 2, 8, 10] },
                                { x: 3, y: [2, 8, 6, 5] },
                                { x: 4, y: [1, 3, 2, 9] }
                            ]}
                        />
                    </VictoryChart>)
        }else if(props.id == 2 || props.id == 6){
            body = (<VictoryChart>
                        <VictoryArea
                        style={{ data: { fill: "#c43a31" } }}
                        data={[
                            { x: 1, y: 2, y0: 0 },
                            { x: 2, y: 3, y0: 1 },
                            { x: 3, y: 5, y0: 1 },
                            { x: 4, y: 4, y0: 2 },
                            { x: 5, y: 6, y0: 2 }
                          ]}
                        />
                    </VictoryChart>)
        }else if(props.id == 3 || props.id == 4 ||  props.id == 7){
            body = (<VictoryChart>
                <VictoryGroup offset={20}
                  colorScale={"qualitative"}
                >
                  <VictoryBar
                    data={[{ x: 1, y: 1 }, { x: 2, y: 2 }, { x: 3, y: 5 }]}
                  />
                  <VictoryBar
                    data={[{ x: 1, y: 2 }, { x: 2, y: 1 }, { x: 3, y: 7 }]}
                  />
                  <VictoryBar
                    data={[{ x: 1, y: 3 }, { x: 2, y: 4 }, { x: 3, y: 9 }]}
                  />
                </VictoryGroup>
              </VictoryChart>)
        }else{
            body = ( <VictoryChart>
                    <VictoryLine
                    style={{
                        data: { stroke: "#c43a31" },
                        parent: { border: "1px solid #ccc"}
                    }}
                    data={[
                        { x: 1, y: 2 },
                        { x: 2, y: 3 }]}
                    />
            </VictoryChart> )
        }

        return body;
    }

    return (
        <div>
               {getGraph()}
        </div>
    )
}

index.propTypes = {

}

export default index


