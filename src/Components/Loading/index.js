import React from 'react'
import PropTypes from 'prop-types'
import { Progress } from 'antd';
import { Row, Col , Card } from 'antd';
import { Form, Icon, Input, Button,Checkbox } from 'antd';
import {Animated} from "react-animated-css";

function index(props) {
    return (
        <Animated animationIn="fadeIn" animationOut="fadeOut" animationInDuration={5000} animationOutDuration={2000} isVisible={props.status}
        style={{width:"100%", height:"100%" , zIndex: '10' , position:"absolute" , left:0 , right:0,background:"white"}} 
        >
            <Row type="flex" justify="space-around" align="middle" style={{width:"100%", height:"100%"}} >
                <Col span={1} md={2} xl={6}></Col>
                <Col span={14} md={12} xl={6}>      
                    <center><img src="./img/giphy.gif" width="180" height="150"  /></center>
                    <Progress  
                        percent={props.value}
                        status="active"
                    />
                </Col>
                <Col span={1} md={2} xl={6}></Col>
            </Row>
        </Animated>
       
    )
}


export default index;

