
### Build ###

docker build -t "name image" -f DockerFile.DockerFile

### Deploy ###

docker run -d -p 80:80 "name image"
